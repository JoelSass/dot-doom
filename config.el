;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!
(setq user-full-name "Joel Saß"
      user-mail-address "joelpaulsass@gmail.com")

(setq doom-theme 'doom-solarized-light)

(setq org-directory "~/MEGA/org/")
asdf

(use-package! org-roam
:hook
(after-init . org-roam-mode)
:custom
(org-roam-directory "~/MEGA/org-roam/")
:bind (:map org-roam-mode-map
(("C-c n l" . org-roam)
 ("C-c n f" . org-roam-find-file)
 ("C-c n g" . org-roam-graph))
:map org-mode-map
(("C-c n i" . org-roam-insert))
(("C-c n I" . org-roam-insert-immediate))))

(setq org-roam-capture-templates
'(("d" "default" plain (function org-roam-capture--get-point)
"%?"
:file-name "${slug}"
:head "#+TITLE: ${title}
#+startup: latexpreview showall

#+ROAM_ALIAS:
#+CREATED: %u

- tags ::

\* ${title}
:PROPERTIES:
:ANKI_DECK: Master
:ANKI_NOTE_TYPE: Basic
:END:
\** Front
    ${title}
\** Back
"
:unnarrowed t
:immediate-finish t)))

(defun push-anki-h()
  (when (org-roam--org-roam-file-p)
    (anki-editor-push-notes)))

(add-hook 'after-save-hook 'push-anki-h)

(use-package! anki-editor
  :after org
  :bind (:map org-mode-map
              ("<f12>" . anki-editor-cloze-region-auto-incr)
              ("<f11>" . anki-editor-cloze-region-dont-incr)
              ("<f10>" . anki-editor-reset-cloze-number)
              ("<f9>"  . anki-editor-push-notes))
  :hook (org-capture-after-finalize . anki-editor-reset-cloze-number) ; Reset cloze-number after each capture.
  :config
  (setq anki-editor-create-decks t ;; Allow anki-editor to create a new deck if it doesn't exist
        anki-editor-org-tags-as-anki-tags t)

  (defun anki-editor-cloze-region-auto-incr (&optional arg)
    "Cloze region without hint and increase card number."
    (interactive)
    (anki-editor-cloze-region my-anki-editor-cloze-number "")
    (setq my-anki-editor-cloze-number (1+ my-anki-editor-cloze-number))
    (forward-sexp))
  (defun anki-editor-cloze-region-dont-incr (&optional arg)
    "Cloze region without hint using the previous card number."
    (interactive)
    (anki-editor-cloze-region (1- my-anki-editor-cloze-number) "")
    (forward-sexp))
  (defun anki-editor-reset-cloze-number (&optional arg)
    "Reset cloze number to ARG or 1"
    (interactive)
    (setq my-anki-editor-cloze-number (or arg 1)))
  (defun anki-editor-push-tree ()
    "Push all notes under a tree."
    (interactive)
    (anki-editor-push-notes '(4))
    (anki-editor-reset-cloze-number))
  ;; Initialize
  (anki-editor-reset-cloze-number)
  )
;; Org-capture templates
(setq org-my-anki-file "~/MEGA/anki/")
(setq org-capture-templates
      '(("a" "Anki basic" entry (file+headline org-my-anki-file "Dispatch Shelf")
         "* %<%H:%M>   %^g\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Basic\n:ANKI_DECK: Mega\n:END:\n** Front\n%?\n** Back\n%x\n")
        ("A" "Anki cloze" entry (file+datetree org-my-anki-file "Dispatch Shelf")
         "* %<%H:%M>   %^g\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Cloze\n:ANKI_DECK: Mega\n:END:\n** Text\n%x\n** Extra\n")))


;; Allow Emacs to access content from clipboard.
(setq x-select-enable-clipboard t
x-select-enable-primary t)

(use-package! lsp-ui
:ensure t
:after lsp-mode
:diminish
:commands lsp-ui-mode
:custom-face
(lsp-ui-doc-background ((t (:background nil))))
(lsp-ui-doc-header ((t (:inherit (font-lock-string-face italic)))))
:bind
(:map lsp-ui-mode-map
([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
([remap xref-find-references] . lsp-ui-peek-find-references)
("C-c u" . lsp-ui-imenu)
("M-i" . lsp-ui-doc-focus-frame))
(:map lsp-mode-map
("M-n" . forward-paragraph)
("M-p" . backward-paragraph))
:custom
(lsp-ui-doc-header t)
(lsp-ui-doc-include-signature t)
(lsp-ui-doc-border (face-foreground 'default))
(lsp-ui-sideline-enable nil)
(lsp-ui-sideline-ignore-duplicate t)
(lsp-ui-sideline-show-code-actions nil)
:config
(setq lsp-ui-doc-use-webkit t)
;; WORKAROUND Hide mode-line of the lsp-ui-imenu buffer
;; https://github.com/emacs-lsp/lsp-ui/issues/243
(defadvice lsp-ui-imenu (after hide-lsp-ui-imenu-mode-line activate)
(setq mode-line-format nil)))

(setq display-line-numbers-type t)

(show-paren-mode t)
(setq show-paren-style 'expression)
(fset 'yes-or-no-p 'y-or-n-p)
(display-time-mode 1)
(display-battery-mode 1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

(use-package! doom-themes
:ensure t
:custom-face
(cursor ((t (:background "dark goldenrod"))))
:config
;; flashing mode-line on errors
(doom-themes-visual-bell-config)
;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)
(load-theme 'doom-solarized-light t))


(use-package! doom-modeline
:ensure t
:custom
;; Don't compact font caches during GC. Windows Laggy Issue
(inhibit-compacting-font-caches t)
(doom-modeline-minor-modes t)
(doom-modeline-icon t)
(doom-modeline-major-mode-color-icon t)
(doom-modeline-height 15)
:config
(doom-modeline-mode))

(after! lsp-ui (setq lsp-ui-doc-enable t))

(setq make-backup-files nil)

(add-load-path! "/usr/share/emacs/site-lisp/mu4e")

(setq +mu4e-backend 'offlineimap)

(defvar org-msg-currently-exporting nil
  "Helper variable to indicate whether org-msg is currently exporting the org buffer to HTML.
Usefull for affecting some of my HTML export config.")

(use-package! org-msg
  :after mu4e
  :config
  (setq org-msg-options "html-postamble:nil H:5 num:nil ^:{} toc:nil author:nil email:nil \\n:t tex:dvipng"
        org-msg-startup "hidestars indent inlineimages"
        org-msg-greeting-fmt "\nHi %s,\n\n"
        org-msg-greeting-name-limit 3
        org-msg-text-plain-alternative t)
  (map! :map org-msg-edit-mode-map
        :n "G" #'org-msg-goto-body)
  (defadvice! org-msg--now-exporting (&rest _)
    :before #'org-msg-org-to-xml
    (setq org-msg-currently-exporting t))
  (defadvice! org-msg--not-exporting (&rest _)
    :after #'org-msg-org-to-xml
    (setq org-msg-currently-exporting nil))
  (setq org-msg-enforce-css
        (let* ((font-family '(font-family . "-apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Oxygen, Ubuntu, Cantarell,\
          \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\";"))
               (monospace-font '(font-family . "SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;"))
               (font-size '(font-size . "11pt"))
               (font `(,font-family ,font-size))
               (line-height '(line-height . "1.2"))
               (theme-color "#2654BF")
               (bold '(font-weight . "bold"))
               (color `(color . ,theme-color))
               (table `((margin-top . "6px") (margin-bottom . "6px")
                        (border-left . "none") (border-right . "none")
                        (border-top . "2px solid #222222") (border-bottom . "2px solid #222222")
                        ))
               (ftl-number `(,color ,bold (text-align . "left")))
               (inline-modes '(asl c c++ conf cpp csv diff ditaa emacs-lisp
                                   fundamental ini json makefile man org plantuml
                                   python sh xml))
               (inline-src `((background-color . "rgba(27,31,35,.05)")
                             (border-radius . "3px")
                             (padding . ".2em .4em")
                             (font-size . "90%") ,monospace-font
                             (margin . 0)))
               (code-src
                (mapcar (lambda (mode)
                          `(code ,(intern (concat "src src-" (symbol-name mode)))
                                 ,inline-src))
                        inline-modes)))
          `((del nil ((color . "grey") (border-left . "none")
                      (text-decoration . "line-through") (margin-bottom . "0px")
                      (margin-top . "10px") (line-height . "11pt")))
            (a nil (,color))
            (a reply-header ((color . "black") (text-decoration . "none")))
            (div reply-header ((padding . "3.0pt 0in 0in 0in")
                               (border-top . "solid #e1e1e1 1.0pt")
                               (margin-bottom . "20px")))
            (span underline ((text-decoration . "underline")))
            (li nil (,line-height (margin-bottom . "0px")
                                  (margin-top . "2px")))
            (nil org-ul ((list-style-type . "square")))
            (nil org-ol (,@font ,line-height (margin-bottom . "0px")
                                (margin-top . "0px") (margin-left . "30px")
                                (padding-top . "0px") (padding-left . "5px")))
            (nil signature (,@font (margin-bottom . "20px")))
            (blockquote nil ((padding . "0px 10px") (margin-left . "10px")
                             (margin-top . "20px") (margin-bottom . "0")
                             (border-left . "3px solid #ccc") (font-style . "italic")
                             (background . "#f9f9f9")))
            (code nil (,font-size ,monospace-font (background . "#f9f9f9")))
            ,@code-src
            (nil linenr ((padding-right . "1em")
                         (color . "black")
                         (background-color . "#aaaaaa")))
            (pre nil ((line-height . "1.2")
                      (color . ,(doom-color 'fg))
                      (background-color . ,(doom-color 'bg))
                      (margin . "4px 0px 8px 0px")
                      (padding . "8px 12px")
                      (width . "95%")
                      (border-radius . "5px")
                      (font-weight . "500")
                      ,monospace-font))
            (div org-src-container ((margin-top . "10px")))
            (nil figure-number ,ftl-number)
            (nil table-number)
            (caption nil ((text-align . "left")
                          (background . ,theme-color)
                          (color . "white")
                          ,bold))
            (nil t-above ((caption-side . "top")))
            (nil t-bottom ((caption-side . "bottom")))
            (nil listing-number ,ftl-number)
            (nil figure ,ftl-number)
            (nil org-src-name ,ftl-number)
            (img nil ((vertical-align . "middle")
                      (max-width . "100%")))
            (img latex-fragment-inline ((transform . ,(format "translateY(-1px) scale(%.3f)"
                                                       (/ 1.0 (if (boundp 'preview-scale)
                                                                  preview-scale 1.4))))
                                 (margin . "0 -0.35em")))
            (table nil (,@table ,line-height (border-collapse . "collapse")))
            (th nil ((border . "none") (border-bottom . "1px solid #222222")
                     (background-color . "#EDEDED") (font-weight . "500")
                     (padding . "3px 10px")))
            (td nil (,@table (padding . "1px 10px")
                             (background-color . "#f9f9f9") (border . "none")))
            (td org-left ((text-align . "left")))
            (td org-right ((text-align . "right")))
            (td org-center ((text-align . "center")))
            (kbd nil ((border . "1px solid #d1d5da") (border-radius . "3px")
                      (box-shadow . "inset 0 -1px 0 #d1d5da") (background-color . "#fafbfc")
                      (color . "#444d56") (padding . "3px 5px") (display . "inline-block")))
            (div outline-text-4 ((margin-left . "15px")))
            (div outline-4 ((margin-left . "10px")))
            (h4 nil ((margin-bottom . "0px") (font-size . "11pt")))
            (h3 nil ((margin-bottom . "0px")
                     ,color (font-size . "14pt")))
            (h2 nil ((margin-top . "20px") (margin-bottom . "20px")
                     ,color (font-size . "18pt")))
            (h1 nil ((margin-top . "20px")
                     (margin-bottom . "0px") ,color (font-size . "24pt")))
            (p nil ((text-decoration . "none") (margin-bottom . "0px")
                    (margin-top . "10px") (line-height . "11pt") ,font-size
                    (max-width . "100ch")))
            (b nil ((font-weight . "500") (color . ,theme-color)))
            (div nil (,@font (line-height . "12pt"))))))
  
  (org-msg-mode t))

(setq org-msg-enforce-css
      (let* ((font-family '(font-family . "-apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Oxygen, Ubuntu, Cantarell,\
        \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\";"))
             (monospace-font '(font-family . "SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace;"))
             (font-size '(font-size . "11pt"))
             (font `(,font-family ,font-size))
             (line-height '(line-height . "1.2"))
             (theme-color "#2654BF")
             (bold '(font-weight . "bold"))
             (color `(color . ,theme-color))
             (table `((margin-top . "6px") (margin-bottom . "6px")
                      (border-left . "none") (border-right . "none")
                      (border-top . "2px solid #222222") (border-bottom . "2px solid #222222")
                      ))
             (ftl-number `(,color ,bold (text-align . "left")))
             (inline-modes '(asl c c++ conf cpp csv diff ditaa emacs-lisp
                                 fundamental ini json makefile man org plantuml
                                 python sh xml))
             (inline-src `((background-color . "rgba(27,31,35,.05)")
                           (border-radius . "3px")
                           (padding . ".2em .4em")
                           (font-size . "90%") ,monospace-font
                           (margin . 0)))
             (code-src
              (mapcar (lambda (mode)
                        `(code ,(intern (concat "src src-" (symbol-name mode)))
                               ,inline-src))
                      inline-modes)))
        `((del nil ((color . "grey") (border-left . "none")
                    (text-decoration . "line-through") (margin-bottom . "0px")
                    (margin-top . "10px") (line-height . "11pt")))
          (a nil (,color))
          (a reply-header ((color . "black") (text-decoration . "none")))
          (div reply-header ((padding . "3.0pt 0in 0in 0in")
                             (border-top . "solid #e1e1e1 1.0pt")
                             (margin-bottom . "20px")))
          (span underline ((text-decoration . "underline")))
          (li nil (,line-height (margin-bottom . "0px")
                                (margin-top . "2px")))
          (nil org-ul ((list-style-type . "square")))
          (nil org-ol (,@font ,line-height (margin-bottom . "0px")
                              (margin-top . "0px") (margin-left . "30px")
                              (padding-top . "0px") (padding-left . "5px")))
          (nil signature (,@font (margin-bottom . "20px")))
          (blockquote nil ((padding . "0px 10px") (margin-left . "10px")
                           (margin-top . "20px") (margin-bottom . "0")
                           (border-left . "3px solid #ccc") (font-style . "italic")
                           (background . "#f9f9f9")))
          (code nil (,font-size ,monospace-font (background . "#f9f9f9")))
          ,@code-src
          (nil linenr ((padding-right . "1em")
                       (color . "black")
                       (background-color . "#aaaaaa")))
          (pre nil ((line-height . "1.2")
                    (color . ,(doom-color 'fg))
                    (background-color . ,(doom-color 'bg))
                    (margin . "4px 0px 8px 0px")
                    (padding . "8px 12px")
                    (width . "95%")
                    (border-radius . "5px")
                    (font-weight . "500")
                    ,monospace-font))
          (div org-src-container ((margin-top . "10px")))
          (nil figure-number ,ftl-number)
          (nil table-number)
          (caption nil ((text-align . "left")
                        (background . ,theme-color)
                        (color . "white")
                        ,bold))
          (nil t-above ((caption-side . "top")))
          (nil t-bottom ((caption-side . "bottom")))
          (nil listing-number ,ftl-number)
          (nil figure ,ftl-number)
          (nil org-src-name ,ftl-number)
          (img nil ((vertical-align . "middle")
                    (max-width . "100%")))
          (img latex-fragment-inline ((transform . ,(format "translateY(-1px) scale(%.3f)"
                                                     (/ 1.0 (if (boundp 'preview-scale)
                                                                preview-scale 1.4))))
                               (margin . "0 -0.35em")))
          (table nil (,@table ,line-height (border-collapse . "collapse")))
          (th nil ((border . "none") (border-bottom . "1px solid #222222")
                   (background-color . "#EDEDED") (font-weight . "500")
                   (padding . "3px 10px")))
          (td nil (,@table (padding . "1px 10px")
                           (background-color . "#f9f9f9") (border . "none")))
          (td org-left ((text-align . "left")))
          (td org-right ((text-align . "right")))
          (td org-center ((text-align . "center")))
          (kbd nil ((border . "1px solid #d1d5da") (border-radius . "3px")
                    (box-shadow . "inset 0 -1px 0 #d1d5da") (background-color . "#fafbfc")
                    (color . "#444d56") (padding . "3px 5px") (display . "inline-block")))
          (div outline-text-4 ((margin-left . "15px")))
          (div outline-4 ((margin-left . "10px")))
          (h4 nil ((margin-bottom . "0px") (font-size . "11pt")))
          (h3 nil ((margin-bottom . "0px")
                   ,color (font-size . "14pt")))
          (h2 nil ((margin-top . "20px") (margin-bottom . "20px")
                   ,color (font-size . "18pt")))
          (h1 nil ((margin-top . "20px")
                   (margin-bottom . "0px") ,color (font-size . "24pt")))
          (p nil ((text-decoration . "none") (margin-bottom . "0px")
                  (margin-top . "10px") (line-height . "11pt") ,font-size
                  (max-width . "100ch")))
          (b nil ((font-weight . "500") (color . ,theme-color)))
          (div nil (,@font (line-height . "12pt"))))))


(after! org-superstar
  (setq org-superstar-headline-bullets-list '("" "" "" "" "" "" "" "")
        org-superstar-prettify-item-bullets t ))

(require 'dashboard)
(dashboard-setup-startup-hook)
(setq dashboard-items '((bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)))
(setq dashboard-startup-banner "~/Downloads/e_z_money.png")
(setq show-week-agenda-p t)

(custom-set-faces
'(org-level-1 ((t (:height 2.0 :foreground "#a71d31"))))
'(org-level-2 ((t (:height 1.5 :foreground "#8D6B94"))))
'(org-level-3 ((t (:height 1.25 ))))
'(org-level-4 ((t (:height 1.15 ))))
)
