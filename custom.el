(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elfeed-feeds
   '("https://www.mcafee.com/blogs/feed/" "http://feeds.feedburner.com/eset/blog"))
 '(smtpmail-smtp-server "smtp.googlemail.com")
 '(smtpmail-smtp-service 25))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cursor ((t (:background "dark goldenrod"))))
 '(org-level-1 ((t (:height 2.0 :foreground "#a71d31"))))
 '(org-level-2 ((t (:height 1.5 :foreground "#8D6B94"))))
 '(org-level-3 ((t (:height 1.25))))
 '(org-level-4 ((t (:height 1.15)))))
